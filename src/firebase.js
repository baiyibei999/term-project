import firebase from 'firebase';
import firestore from 'firebase/firestore';

//const settings = {timestampsInSnapshots: true};
const config = {
    apiKey: "AIzaSyBxprPJrOC2XznQw2mK-2IjZOyePc3rzLM",
    authDomain: "fir-reactt.firebaseapp.com",
    databaseURL: "https://fir-reactt.firebaseio.com",
    projectId: "fir-reactt",
    storageBucket: "fir-reactt.appspot.com",
    messagingSenderId: "76296914792",
    appId: "1:76296914792:web:acbb31ca6efca1956878dd"
  };
 
// initialize the firebase of the provided constant key
firebase.initializeApp(config);
//firebase.firestore().settings(settings);
// firestore
const db = firebase.firestore();
export { db };
// firebase
export default firebase;
