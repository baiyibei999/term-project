import React from 'react';
import { userService } from '@/_services';
import { db } from '../firebase';
import {firebase} from '../firebase';
import { history} from '@/_helpers';
import { Button} from 'reactstrap';

//import Contacts from '../AdminActions/Contacts';
class Semester2 extends React.Component {
    constructor(props) {
        super(props);
        this.ref = db.collection('semester2');
        this.ref2 = db.collection('myActivity2');
        this.unsubscribe1 = null;
        this.unsubscribe2 = null;
        this.state = {
            users: null,
            semester2:[],
            myActivity2:[],
            id:'',
            organizer:'',
            numberOfStudents:'',
            academicYear:'',
            semester:'',
            name: '',
            time: '',
            room: '',
            credit: '',
            hours:'',
            key:'',
            display_name: 'block'
        };
    } 
   
    onCollectionUpdate1 = (querySnapshot) => {
        const semester2 = [];
        querySnapshot.forEach((doc) => {
          const { name, time,key,credit, hours,room,id,organizer ,numberOfStudents,academicYear,semester} = doc.data();
          semester2.push({
            key:doc.id,
            id,
            organizer,
            doc ,
            key,
            name, 
            time,
            room,
            numberOfStudents,
            academicYear,
            hours,
            credit,
            semester// DocumentSnapshot
        
          });
          console.log('hello', doc.data());
        });
        this.setState({
          users:userService.getAll(),
          semester2
       });
      }

      onCollectionUpdate2 = (querySnapshot) => {
        const myActivity2 = [];
        querySnapshot.forEach((doc) => {
          const { name, time,credit,key, hours,room,id,organizer ,numberOfStudents,academicYear,semester} = doc.data();
          myActivity2.push({
            key,
            id,
            organizer,
            doc ,
            name, 
            time,
            room,
            numberOfStudents,
            academicYear,
            hours,
            credit,
            semester// DocumentSnapshot
        
          });
          
          console.log('hello', doc.data());
        });
        this.setState({
            myActivity2,
         
          
       });
      }


      componentDidMount() {
        this.unsubscribe1 = this.ref.onSnapshot(this.onCollectionUpdate1);
        this.unsubscribe2 = this.ref2.onSnapshot(this.onCollectionUpdate2);
    }
    
    render() {
        const { semester2 } = this.state;
        const { myActivity2 } = this.state;
        let hoursSum = 0;
        const list = _.groupBy(myActivity2)
        _.forEach(list , (activity) => {
            _.forEach(activity, (value) => {
                const hours = _.toInteger(value.hours);
                hoursSum += hours;
            })
        });
        return (

    
   <div class="container" >
      <h3 >
              Avaliable LIST
            </h3>
        <div class="col-md-3">
       
         
          <table className="table table-borderless table-stripped">
          
                        <thead className="thead-light">
                            
                            <tr>
                            <th >Id</th>
                            <th>Name</th>
                            <th>AcademicYear</th>
                           <th>Semester</th>
                            <th>Time</th>
                            <th>Hourse</th>
                            <th>Room</th>
                          <th>NumOfStudents</th>
                          <th>Leader</th>
                          <th>Credit</th>
                            </tr>
                           
                        </thead>
                        <tbody>
                        {semester2.map(board =>
                
                   <tr>
                     <td>{board.id} </td>
                     <td>{board.name}</td>
                     <td>{board.academicYear}</td>
                     <td>{board.semester}</td>
                     <td>{board.time}</td>
                     <td>{board.hours}</td>
                     <td>{board.room}</td>
                     <td>{board.numberOfStudents}</td>
                     <td>{board.organizer}</td>
                     <td>{board.credit}</td>
                     
                     {hoursSum>50||hoursSum===50?<Button type="button" disabled  onClick={ this.backData2.bind(board)} className="btn btn-warning" >Enrolle</Button>:
                     <Button type="button" onClick={ this.backData2.bind(board)} className="btn btn-warning" >Enrolle</Button>}
                   </tr>
                 
                 )}
                        </tbody>
                    </table>

         </div>
         <hr></hr>
         <h3 >
              My LIST
            </h3>
         <h5>Hours :{hoursSum}/50 </h5>           
        <div class="col-md-3">
       
          
          <table className="table table-borderless table-stripped">
          
                        <thead className="thead-light">
                            
                            <tr>
                            <th >Id</th>
                            <th>Name</th>
                            <th>AcademicYear</th>
                           <th>Semester</th>
                            <th>Time</th>
                            <th>Hourse</th>
                            <th>Room</th>
                          <th>NumOfStudents</th>
                          <th>Leader</th>
                          <th>Credit</th>
                            </tr>
                           
                        </thead>
                        <tbody>
                        {myActivity2.map(board =>
                
                   <tr>
                     <td>{board.id} </td>
                     <td>{board.name}</td>
                     <td>{board.academicYear}</td>
                     <td>{board.semester}</td>
                     <td>{board.time}</td>
                     <td>{board.hours}</td>
                     <td>{board.room}</td>
                     <td>{board.numberOfStudents}</td>
                     <td>{board.organizer}</td>
                     <td>{board.credit}</td>
                     <button onClick={this.delete2.bind(this, board.key)}class="btn btn-danger">Delete</button>
                     {/* <button onClick={ () => this.removeData2(board) } className="btn btn-danger">Delete</button> */}
                   </tr>
                 
                 )}
                        </tbody>
                    </table>

         </div>
      </div>
      

 );

}
backData2 = (e) => {

      
    this.ref2.add({
        id:e.id,
        organizer:e.organizer,
        numberOfStudents:e.numberOfStudents,
        academicYear:e.academicYear,
        semester:e.semester,
        name:e.name,
        time:e.time,
        room:e.room,
        credit: e.credit,
        hours:e.hours,
        key:e.key
    }).then((docRef) => {
      this.setState({
      id:e.id,
      organizer:e.organizer,
      numberOfStudents:e.numberOfStudents,
      academicYear:e.academicYear,
      semester:e.semester,
      name:e.name,
      time:e.time,
      room:e.room,
      credit: e.credit,
      hours:e.hours,
      key:e.key
      });
      this.delete1(e.key);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }
  delete1(id){
    db.collection("semester2").doc(`${id}`).delete();
    this.backData1(board);
  }
  delete2(id){
    db.collection("myActivity2").doc(`${id}`).delete();
    this.backData1(board);
  }

  backData1 = (e) => {

    this.ref.add({
        id:e.id,
        key:e.key,
        organizer:e.organizer,
        numberOfStudents:e.numberOfStudents,
        academicYear:e.academicYear,
        semester:e.semester,
        name:e.name,
        time:e.time,
        room:e.room,
        credit: e.credit,
        hours:e.hours,
        key:e.key
    }).then((docRef) => {
      this.setState({
      id:e.id,
      organizer:e.organizer,
      numberOfStudents:e.numberOfStudents,
      academicYear:e.academicYear,
      semester:e.semester,
      name:e.name,
      time:e.time,
      room:e.room,
      credit: e.credit,
      hours:e.hours,
      key:e.key
      });
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }
removeData1 = (board) => {
    const { semester2 } = this.state;
    const newState = semester2.filter(data => {
      return data.id !== board.id;
    });
    this.setState({ semester2: newState });
  }
  removeData2 = (board) => {
    const { myActivity2 } = this.state;
    const newState = myActivity2.filter(data => {
      return data.id !== board.id;
    });
    this.setState({ myActivity2: newState });
    this.backData1(board);
  }
}
export { Semester2 };