import React from 'react';
import { ListGroup, Button} from 'reactstrap';
import { userService } from '@/_services';
import { history} from '@/_helpers';
import { Link } from 'react-router-dom';
class StudentPage extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            users: null
        };
    }

    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    render() {
        const { users } = this.state;
        return (
            
            <div>
                
                <h1>Student</h1>
                <p>This page can only be accessed by students.</p>
      <ListGroup>
        
        <Button color="info" onClick={() => history.push('/semester1')}>Semester 1</Button>
        <Button color="success" onClick={() => history.push('/semester2')}>Semester 2</Button>
      </ListGroup>
    </div>
            
        );
    }
}

export { StudentPage };