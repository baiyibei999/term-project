import React from 'react';
import { userService } from '@/_services';
import { db } from '../firebase';
import {firebase} from '../firebase';
import { history} from '@/_helpers';
import { Button} from 'reactstrap';

//import Contacts from '../AdminActions/Contacts';
class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.ref = db.collection('activities');
        this.unsubscribe = null;
        this.state = {
            users: null,
            activities:[]
        };
    }
    delete(id){
        db.collection('activities').doc(id).delete().then(()=> {
        console.log("Document successfully deleted!");
        this.props.history.push("/admin")
        }).catch((error) => {
        console.error("Error removing document: ", error);
        });
        }
    onCollectionUpdate = (querySnapshot) => {
        const activities = [];
        querySnapshot.forEach((doc) => {
          const { name, time,credit, hours,room,id,organizer ,numOfStudents,academicYear,semester} = doc.data();
          activities.push({
            key:doc.id,
            id,
            organizer,
            doc ,
            credit,
            name, 
            time,
            room,
            numOfStudents,
            academicYear,
            hours,
            semester// DocumentSnapshot
        
          });
          console.log('hello', doc.data());
        });
        this.setState({
          users:userService.getAll(),
          activities
          
       });
      }


    componentDidMount() {
        this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
    }
    
    render() {
        const { activities } = this.state;

        return (

    
   <div class="container" >
      <h1 >
              Activity LIST
            </h1>
        <div class="col-md-3">
       
          <Button color="info" onClick={() => history.push('/create')}>create</Button>
          <table className="table table-borderless table-stripped">
          
                        <thead className="thead-light">
                            
                            <tr>
                            <th >Id</th>
                            <th>Name</th>
                            <th>AcademicYear</th>
                           <th>Semester</th>
                            <th>Time</th>
                            <th>Hourse</th>
                            <th>Room</th>
                          <th>NumOfStudent</th>
                          <th>Leader</th>
                          <th>Credit</th>
                            </tr>
                           
                        </thead>
                        <tbody>
                        {activities.map(board =>
                
                   <tr>
                     <td>{board.id} </td>
                     <td>{board.name}</td>
                     <td>{board.academicYear}</td>
                     <td>{board.semester}</td>
                     <td>{board.time}</td>
                     <td>{board.hours}</td>
                     <td>{board.room}</td>
                     <td>{board.numOfStudents}</td>
                     <td>{board.organizer}</td>
                     <td>{board.credit}</td>
                     
                     <button onClick={ () => this.removeData(board) } className="btn btn-danger">Delete</button>
                   </tr>
                 
                 )}
                        </tbody>
                    </table>

         </div>
      </div>

 );
}
removeData = (board) => {
    const { activities } = this.state;
    const newState = activities.filter(data => {
      return data.id !== board.id;
    });
    this.setState({ activities: newState });
  }
  removeItem(itemId) {
    const itemRef = firebase.database().ref(`/activities/${itemId}`);
    itemRef.remove();
  }
  delete(board){
    db.collection('activities').doc(board).delete().then(() => {
      console.log("Document successfully deleted!");
      this.props.history.push("/admin")
    }).catch((error) => {
      console.error("Error removing document: ", error);
    });
  }
}
export { AdminPage };