import React from 'react';
import { userService } from '@/_services';
import { db } from '../firebase';
import {firebase} from '../firebase';
import { history} from '@/_helpers';
import { Button} from 'reactstrap';

//import Contacts from '../AdminActions/Contacts';
class Semester1 extends React.Component {
    constructor(props) {
        super(props);
        this.ref = db.collection('activities');
        this.ref2 = db.collection('myActivity');
        this.unsubscribe1 = null;
        this.unsubscribe2 = null;
        this.state = {
            users: null,
            activities:[],
            myActivity:[],
            id:'',
            organizer:'',
            numOfStudents:'',
            academicYear:'',
            semester:'',
            name: '',
            time: '',
            room: '',
            credit: '',
            hours:'',
            key:'',
            times:0
        };
    }
    onCollectionUpdate1 = (querySnapshot) => {
        const activities = [];
        querySnapshot.forEach((doc) => {
          const { name, time,key,credit, hours,room,id,organizer ,numOfStudents,academicYear,semester} = doc.data();
          activities.push({
            key:doc.id,
            id,
            organizer,
            doc ,
            key,
            name, 
            time,
            room,
            numOfStudents,
            academicYear,
            hours,
            credit,
            semester// DocumentSnapshot
        
          });
          console.log('hello', doc.data());
        });
        this.setState({
          users:userService.getAll(),
          activities
       });
      }

      onCollectionUpdate2 = (querySnapshot) => {
        const myActivity = [];
        querySnapshot.forEach((doc) => {
          const { name, time,credit,key, hours,room,id,organizer ,numOfStudents,academicYear,semester} = doc.data();
          myActivity.push({
            key,
            id,
            organizer,
            doc ,
            name, 
            time,
            room,
            numOfStudents,
            academicYear,
            hours,
            credit,
            semester// DocumentSnapshot
        
          });
          console.log('hello', doc.data());
        });
        this.setState({
            myActivity
          
       });
      }


      componentDidMount() {
        this.unsubscribe1 = this.ref.onSnapshot(this.onCollectionUpdate1);
        this.unsubscribe2 = this.ref2.onSnapshot(this.onCollectionUpdate2);
    }
    
   
        render() {
          const { activities } = this.state;
          const { myActivity } = this.state;
          let hoursSum = 0;
          const list = _.groupBy(myActivity)
          _.forEach(list , (activity) => {
              _.forEach(activity, (value) => {
                  const hours = _.toInteger(value.hours);
                  hoursSum += hours;
              })
          });
        return (

    
   <div class="container" >
      <h3 >
              Avaliable LIST
            </h3>
        <div class="col-md-3">
       
         
          <table className="table table-borderless table-stripped">
          
                        <thead className="thead-light">
                            
                            <tr>
                            <th >Id</th>
                            <th>Name</th>
                            <th>AcademicYear</th>
                           <th>Semester</th>
                            <th>Time</th>
                            <th>Hourse</th>
                            <th>Room</th>
                          <th>NumOfStudent</th>
                          <th>Leader</th>
                          <th>Credit</th>
                            </tr>
                           
                        </thead>
                        <tbody>
                        {activities.map(board =>
                
                   <tr>
                     <td>{board.id} </td>
                     <td>{board.name}</td>
                     <td>{board.academicYear}</td>
                     <td>{board.semester}</td>
                     <td>{board.time}</td>
                     <td>{board.hours}</td>
                     <td>{board.room}</td>
                     <td>{board.numOfStudents}</td>
                     <td>{board.organizer}</td>
                     <td>{board.credit}</td>
                     
                     <button onClick={ () => this.backData2(board)} className="btn btn-warning" >Enrolle</button>
                   </tr>
                 
                 )}
                        </tbody>
                    </table>

         </div>
         <hr></hr>
         <h3 >
              My LIST
            </h3>
         <h5>Hours :{hoursSum}/50 </h5>           
        <div class="col-md-3">
       
          
          <table className="table table-borderless table-stripped">
          
                        <thead className="thead-light">
                            
                            <tr>
                            <th >Id</th>
                            <th>Name</th>
                            <th>AcademicYear</th>
                           <th>Semester</th>
                            <th>Time</th>
                            <th>Hourse</th>
                            <th>Room</th>
                          <th>NumOfStudent</th>
                          <th>Leader</th>
                          <th>Credit</th>
                            </tr>
                           
                        </thead>
                        <tbody>
                        {myActivity.map(board =>
                
                   <tr>
                     <td>{board.id} </td>
                     <td>{board.name}</td>
                     <td>{board.academicYear}</td>
                     <td>{board.semester}</td>
                     <td>{board.time}</td>
                     <td>{board.hours}</td>
                     <td>{board.room}</td>
                     <td>{board.numOfStudents}</td>
                     <td>{board.organizer}</td>
                     <td>{board.credit}</td>
                     <button onClick={this.delete2.bind(this, board.key)} class="btn btn-danger">Delete</button>
                     {/* <button onClick={ () => this.removeData2(board) } className="btn btn-danger">Delete</button> */}
                   </tr>
                 
                 )}
                        </tbody>
                    </table>

         </div>
      </div>
      

 );

}
backData2 = (e) => {

    this.ref2.add({
        id:e.id,
        organizer:e.organizer,
        numOfStudents:e.numOfStudents,
        academicYear:e.academicYear,
        semester:e.semester,
        name:e.name,
        time:e.time,
        room:e.room,
        credit: e.credit,
        hours:e.hours,
        key:e.key
    }).then((docRef) => {
      this.setState({
      id:e.id,
      organizer:e.organizer,
      numOfStudents:e.numOfStudents,
      academicYear:e.academicYear,
      semester:e.semester,
      name:e.name,
      time:e.time,
      room:e.room,
      credit: e.credit,
      hours:e.hours,
      key:e.key
      });
      this.delete1(e.key);
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }
  delete1(id){
    db.collection("activities").doc(`${id}`).delete();
    this.backData1(board);
  }
  delete2(id){
    db.collection("myActivity").doc(`${id}`).delete();
    this.backData1(board);
  }

  backData1 = (e) => {

    this.ref.add({
        id:e.id,
        key:e.key,
        organizer:e.organizer,
        numOfStudents:e.numOfStudents,
        academicYear:e.academicYear,
        semester:e.semester,
        name:e.name,
        time:e.time,
        room:e.room,
        credit: e.credit,
        hours:e.hours,
        key:e.key
    }).then((docRef) => {
      this.setState({
      id:e.id,
      organizer:e.organizer,
      numOfStudents:e.numOfStudents,
      academicYear:e.academicYear,
      semester:e.semester,
      name:e.name,
      time:e.time,
      room:e.room,
      credit: e.credit,
      hours:e.hours,
      key:e.key
      });
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }
removeData1 = (board) => {
    const { activities } = this.state;
    const newState = activities.filter(data => {
      return data.id !== board.id;
    });
    this.setState({ activities: newState });
  }
  removeData2 = (board) => {
    const { myActivity } = this.state;
    const newState = myActivity.filter(data => {
      return data.id !== board.id;
    });
    this.setState({ myActivity: newState });
    this.backData1(board);
  }
}
export { Semester1 };