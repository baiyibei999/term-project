import React from 'react';
import { db } from '../firebase';
import _ from 'lodash';
import { userService } from '@/_services';
import { Container, Row, Col } from 'reactstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import { Table } from 'reactstrap';

const firestoreRef = db.collection('studentList');
class TeacherPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            studentList: [],
            selectedRow: [],
            users: null,
            selectedValue: '602115500'
        };
    }

    componentDidMount() {
        //userService.getAll().then(users => this.setState({ users }));
        this.setState({
            users: userService.getAll(),
        });
        firestoreRef.get().then((querySnapshot) => {
            let studentListHelper = []
            _.remove(studentListHelper);
            querySnapshot.forEach((doc) => {
                const { firstName, lastName, studentId, joinedActivites } = doc.data();
                const userId = doc.id;
                _.forEach(joinedActivites, (activity) => {
                    console.log('enrolled act', activity);
                    const activityHours = activity.hours;
                    const studentScore = _.toInteger(activityHours) * 4;
                    studentListHelper.push({ id: userId + activity.id, firstName: firstName, lastName: lastName, studentId: studentId, activityName: activity.name, activityHours: activityHours, activityCredit: activity.credit, activityId: activity.id, studentScore: studentScore, studentGrade: this._gradeStudentByScore(studentScore) })
                })

                this.setState({ studentList: studentListHelper })
            })
        })

    }
    handleChange = (selectedOption) => {
        this.setState({ selectedValue: selectedOption.label });
        console.log(`Option selected:`, selectedOption);
    };

    _gradePointStudentByGrade = (credit, grade) => {
        switch (grade) {
            case 'A':
                return _.toInteger(credit) * 4;
                break;
            case 'B':
                return _.toInteger(credit) * 3;
                break;
            case 'C':
                return _.toInteger(credit) * 2;
                break;
            case 'D':
                return _.toInteger(credit) * 1;
                break;

            default:
                return _.toInteger(credit) * 0;
                break;
        }
    };
    _gradeStudentByScore = (studentScore) => {
        if (studentScore > 80 && studentScore <= 100) {
            return 'A';
        } else if (studentScore > 70) {
            return 'B';
        } else if (studentScore > 60) {
            return 'C';
        } else if (studentScore > 50) {
            return 'D';
        } else {
            return 'F';
        }
    };

    render() {
        const { users, studentList, selectedValue } = this.state;
        console.log('studentList', studentList);
        const studentSummaryList = _.groupBy(studentList, 'studentId')
        const studentIdList = []
        const gradePointCal = []
        let creditSum = 0;
        _.forEach(studentSummaryList, (activity, key) => {
            studentIdList.push({ label: key, value: key });
            _.forEach(activity, (value, i) => {
                const credit = _.toInteger(value.activityCredit);
                creditSum += credit;
                const gradePoint = this._gradePointStudentByGrade(
                    value.activityCredit,
                    value.studentGrade
                );
                gradePointCal.push({
                    credit: credit,
                    gradePoint: gradePoint,
                });
            })
        })

        console.log('gradePointCal', gradePointCal);

        console.log('studentSummaryList', studentSummaryList);

        return (
            <div className="register-container">
                <Container >
                    <Row>
                        <Col>
                            <h3>Student Information</h3>
                            <BootstrapTable
                                data={studentList}
                                search
                                hover
                                headerStyle={{
                                    background: 'white',
                                }}
                            >
                                <TableHeaderColumn
                                    dataField="id"
                                    isKey
                                    hidden
                                    dataAlign="center"
                                >
                                    ID
              </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="studentId"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Student id
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activityName"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Activity name
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activityId"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Activity id
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="studentScore"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Score
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activityCredit"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Credit
              </TableHeaderColumn>
                            </BootstrapTable>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h3>Student Summary</h3>
                            <Select
                                value={selectedValue.label}
                                defaultValue={'602115500'}
                                onChange={this.handleChange}
                                options={studentIdList}
                            />
                            <BootstrapTable
                                data={studentSummaryList[selectedValue]}
                                search
                                hover
                                headerStyle={{
                                    background: 'white',
                                }}
                            >
                                <TableHeaderColumn
                                    dataField="id"
                                    isKey
                                    hidden
                                    dataAlign="center"
                                >
                                    ID
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activityId"
                                    width="80"
                                    dataAlign="center"
                                >
                                    _Id
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activityName"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Activity name
              </TableHeaderColumn>


                                <TableHeaderColumn
                                    dataField="activityCredit"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Credit
              </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="studentGrade"
                                    width="80"
                                    dataAlign="center"
                                >
                                    Grade
              </TableHeaderColumn>
                            </BootstrapTable>
                        </Col>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Summary</th>
                                    <th>Activity: {_.size(studentSummaryList[selectedValue])}</th>
                                    <th>Credit: {creditSum}</th>
                                    <th>GPA: {(
                                        _.sumBy(
                                            gradePointCal,
                                            'gradePoint'
                                        ) /
                                        _.sumBy(
                                            gradePointCal,
                                            'credit'
                                        )
                                    )}</th>
                                </tr>
                            </thead></Table>
                    </Row>
                </Container>

            </div>

        );
    }
}

export { TeacherPage };