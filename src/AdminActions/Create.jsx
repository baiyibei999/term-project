import React, { Component } from 'react';
import db from '../firebase';
class Create extends Component {

  constructor() {
    super();
    this.ref = db.firestore().collection('activities');
    this.state = {
      id:'',
      organizer:'',
      numOfStudents:'',
      academicYear:'',
      semester:'',
      name: '',
      time: '',
      room: '',
      credit: '',
      hours:''
    };
  }
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = (e) => {
    e.preventDefault();

    const {  id,organizer, name, 
      time,
      room,
      numOfStudents,
      academicYear,hours,
      semester,credit } = this.state;

    this.ref.add({
      id,
      organizer,
      credit,
      name, 
      time,
      room,
      hours,
      numOfStudents,
      academicYear,
      semester
    }).then((docRef) => {
      this.setState({
        id:'',
      organizer:'',
      numOfStudents:'',
      academicYear:'',
      semester:'',
      name: '',
      time: '',
      room: '',
      credit: '',
      hours:''
      });
      this.props.history.push("/admin")
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    const { id,
      organizer,
      name, 
      time,
      room,
      hours,
      numOfStudents,
      academicYear,
      credit,
      semester } = this.state;
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              ADD Activity
            </h3>
          </div>
          <div class="panel-body">
            <form onSubmit={this.onSubmit}>
            <div class="form-group">
                <label for="id">Id:</label>
                <input type="text" class="form-control" name="id" value={id} onChange={this.onChange} placeholder="id" />
              </div>
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={name} onChange={this.onChange} placeholder="name" />
              </div>
              <div class="form-group">
                <label for="room">Room:</label>
                <textArea class="form-control" name="room" onChange={this.onChange} placeholder="room" cols="80" rows="3">{room}</textArea>
              </div>
              <div class="form-group">
                <label for="time">Time:</label>
                <input type="text" class="form-control" name="time" value={time} onChange={this.onChange} placeholder="time" />
              </div>
              <div class="form-group">
                <label for="hours">Hours:</label>
                <input type="text" class="form-control" name="hours" value={hours} onChange={this.onChange} placeholder="hours" />
              </div>
              <div class="form-group">
                <label for="credit">Credit:</label>
                <input type="text" class="form-control" name="credit" value={credit} onChange={this.onChange} placeholder="credit" />
              </div>
              <div class="form-group">
                <label for="semester">Semester:</label>
                <input type="text" class="form-control" name="semester" value={semester} onChange={this.onChange} placeholder="semester" />
              </div>
              <div class="form-group">
                <label for="academicYear">AcademicYear:</label>
                <input type="text" class="form-control" name="academicYear" value={academicYear} onChange={this.onChange} placeholder="academicYear" />
              </div>
              <div class="form-group">
                <label for="numOfStudents">NumOfStudents:</label>
                <input type="text" class="form-control" name="numOfStudents" value={numOfStudents} onChange={this.onChange} placeholder="numOfStudents" />
              </div>
              <div class="form-group">
                <label for="organizer">Organizer:</label>
                <input type="text" class="form-control" name="organizer" value={organizer} onChange={this.onChange} placeholder="organizer" />
              </div>
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export {Create};