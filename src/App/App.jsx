import React from 'react';
import { Router, Route, Link } from 'react-router-dom';

import { history, Role } from '@/_helpers';
import { authenticationService } from '@/_services';
import { PrivateRoute } from '@/_components';
import { HomePage } from '@/HomePage';
import { AdminPage } from '@/AdminPage';
import { StudentPage } from '@/StudentPage';
import { MyActivityPage } from '@/MyActivityPage';
import { LoginPage } from '@/LoginPage';
import { TeacherPage } from '@/TeacherPage';
import { Semester1 } from '@/Semester1';
import { Semester2 } from '@/Semester2';
import  {Create} from '@/AdminActions';


class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null,
            isAdmin: false,
            isUser:false,
            isTeacher:false
        };
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({
            currentUser: x,
            isAdmin: x && x.role === Role.Admin,
            isUser: x && x.role === Role.User,
            isTeacher: x && x.role === Role.Teacher
        }));
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    render() {
        const { currentUser, isAdmin ,isUser,isTeacher} = this.state;
        return (
          
        
            <Router history={history}>
                <div>
                    {currentUser &&
                        <nav className="navbar navbar-expand navbar-dark bg-dark">
                            <div className="navbar-nav">
                                <Link to="/" className="nav-item nav-link">Home</Link>
                                {isAdmin && <Link to="/admin" className="nav-item nav-link">Activity</Link>}
                                {isUser && <Link to="/student" className="nav-item nav-link">Activity</Link>}
                                {isTeacher && <Link to="/teacher" className="nav-item nav-link">SetScore</Link>}
                                {/* {isUser && <Link to="/mylist" className="nav-item nav-link">MyActivity</Link>} */}
                             {/*  {isUser && <Link to="/semester1" className="nav-item nav-link">Semester1</Link>}
                                {isUser && <Link to="/semester2" className="nav-item nav-link">Semester2</Link>} */}
                                <a onClick={this.logout} className="nav-item nav-link">Logout</a>
                            </div>
                        </nav>
                    }
                    <div className="jumbotron">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 ">
                                    <PrivateRoute exact path="/" component={HomePage} />
                                    <PrivateRoute path="/admin" roles={[Role.Admin]} component={AdminPage} />
                                    <PrivateRoute path="/student" roles={[Role.User]} component={StudentPage} />
                                    <PrivateRoute path="/teacher" roles={[Role.Teacher]} component={TeacherPage} />
                                    <PrivateRoute path="/mylist" roles={[Role.User]} component={MyActivityPage} />
                                    <PrivateRoute path="/semester1" roles={[Role.User]} component={Semester1} />
                                    <PrivateRoute path="/semester2" roles={[Role.User]} component={Semester2} />
                                    <PrivateRoute path="/create" roles={[Role.Admin]} component={Create} />
                                    <Route path="/login" component={LoginPage} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        );
    }
}

export { App };